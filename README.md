#eav

Entity-attribute-value

Entités dans des vues matérialisées et gestion de celles-ci avec des fonctions écrites automatiquement en PLpgSQL

Attention le SQL présent dans ce répo est destructif, il n'a pas vocation à être exécuté ailleurs que dans le container dans lequel il présent ici.


1 - Installer Docker  
2 - git clone  
3 - cd à l'intérieur du répertoire cloné  
4 - cp .env.dist .env  
5 - Optionnel : modifier le mot de passe dans le .env  
6 - docker compose up à l'intérieur du répertoire cloné  
7 - Ouvrir un SGBDR pour Postgres  
8 - Se connecter en utilisant les données du .env et sur le port 5433  
9 - Exécuter le SQL dans pgsql/code/db/tests/test.sql  

